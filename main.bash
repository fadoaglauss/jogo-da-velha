#!/bin/bash
jogador=1
tabuleiro00="-"
tabuleiro01="-"
tabuleiro02="-"
tabuleiro10="-"
tabuleiro11="-"
tabuleiro12="-"
tabuleiro20="-"
tabuleiro21="-"
tabuleiro22="-"

ganhador=0
vencedor=0
invalida=0
clear
     
while [[ "$ganhador" -ne 1 ]]; do
    
    printf "  0   1   2 \n0 %s | %s | %s \n1 %s | %s | %s \n2 %s | %s | %s \n" $tabuleiro00 $tabuleiro01 $tabuleiro02 $tabuleiro10 $tabuleiro11 $tabuleiro12 $tabuleiro20 $tabuleiro21 $tabuleiro22
    if [[ "$invalida" == 1 ]]; then
        echo "Jogada inválida"
        invalida=0
    
    fi
    read -p "Jogador $jogador: " x y
    
    if [[ "$x" -eq 0  && "$y" -eq 0 ]]; then
        if [[ "$tabuleiro00" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro00="X"
                jogador=2
            else 
                tabuleiro00="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 0  && "$y" -eq 1 ]]; then
        if [[ "$tabuleiro01" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro01="X"
                jogador=2
            else 
                tabuleiro01="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 0  && "$y" -eq 2 ]]; then
        if [[ "$tabuleiro02" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro02="X"
                jogador=2
            else 
                tabuleiro02="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 1  && "$y" -eq 0 ]]; then
        if [[ "$tabuleiro10" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro10="X"
                jogador=2
            else 
                tabuleiro10="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 1  && "$y" -eq 1 ]]; then
        if [[ "$tabuleiro11" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro11="X"
                jogador=2
            else 
                tabuleiro11="O"
                jogador=1
            fi
        else
            invalida=1
            read -p "Jogador $jogador: " x y
        fi
    elif [[ "$x" -eq 1  && "$y" -eq 2 ]]; then
        if [[ "$tabuleiro12" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro12="X"
                jogador=2
            else 
                tabuleiro12="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 2  && "$y" -eq 0 ]]; then
        if [[ "$tabuleiro20" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro20="X"
                jogador=2
            else 
                tabuleiro20="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 2  && "$y" -eq 1 ]]; then
        if [[ "$tabuleiro21" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro21="X"
                jogador=2
            else 
                tabuleiro21="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    elif [[ "$x" -eq 2  && "$y" -eq 2 ]]; then
        if [[ "$tabuleiro22" == "-" ]]; then
            if [[ "$jogador" -eq 1 ]]; then
                tabuleiro22="X"
                jogador=2
            else 
                tabuleiro22="O"
                jogador=1
            fi
        else
            invalida=1
        fi
    else
        invalida=1
        
    fi
        
    
        
    if [[ "$tabuleiro00" == "$tabuleiro01" && "$tabuleiro00" == "$tabuleiro02" ]]; then
        if [[ "$tabuleiro00" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro00" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro10" == "$tabuleiro11" && "$tabuleiro10" == "$tabuleiro12" ]]; then
        if [[ "$tabuleiro10" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro10" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro20" == "$tabuleiro21" && "$tabuleiro20" == "$tabuleiro22" ]]; then
        if [[ "$tabuleiro20" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro20" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro00" == "$tabuleiro11" && "$tabuleiro00" == "$tabuleiro22" ]]; then
        if [[ "$tabuleiro00" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro00" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro20" == "$tabuleiro11" && "$tabuleiro20" == "$tabuleiro02" ]]; then
        if [[ "$tabuleiro20" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro20" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro00" == "$tabuleiro10" && "$tabuleiro00" == "$tabuleiro20" ]]; then
        if [[ "$tabuleiro00" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro00" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro01" == "$tabuleiro11" && "$tabuleiro01" == "$tabuleiro21" ]]; then
        if [[ "$tabuleiro01" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro01" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    elif [[ "$tabuleiro02" == "$tabuleiro12" && "$tabuleiro02" == "$tabuleiro22" ]]; then
        if [[ "$tabuleiro02" == "X" ]]; then
            vencedor=1
            ganhador=1
        elif [[ "$tabuleiro02" == "O" ]]; then
            vencedor=2
            ganhador=1
        fi
    fi
    if [[ "$tabuleiro00" != "-" &&  "$tabuleiro01" != "-" &&  "$tabuleiro02" != "-" && "$tabuleiro10" != "-" && "$tabuleiro11" != "-" && "$tabuleiro12" != "-" && "$tabuleiro20" != "-" && "$tabuleiro21" != "-" && "$tabuleiro22" != "-" ]]; then
        ganhador=1
        vencedor=3
    fi 
    
    clear
    
done


clear
printf "  0   1   2 \n0 %s | %s | %s \n1 %s | %s | %s \n2 %s | %s | %s \n" $tabuleiro00 $tabuleiro01 $tabuleiro02 $tabuleiro10 $tabuleiro11 $tabuleiro12 $tabuleiro20 $tabuleiro21 $tabuleiro22

    
if [[ "$vencedor" -eq 3 ]]; then
    echo "Deu velha"
else 
    echo "Vencedor = Jogador" $vencedor
fi
